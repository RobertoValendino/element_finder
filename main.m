%% Given the mesh of a beam, with dimensions 1x6x1 (m), find the element of the face with ID=3 to which the point P=(0.5,3) belongs
% Read the mesh. This mesh was exported from gmsh in the .m format file.
addpath("Functions\")
name_msh='Mesh\beam3D'
[vertices, boundaries, elements] = msh_to_Mmesh(name_msh);
% Define the boundary ID of interest, this boundary is the one with z=0.
id=3;
% Define the point P of interest.
p.x=0.5;
p.y=3;
% Find the ID of the boundary element to which the point belongs.
i=findboundary(boundaries,vertices,id,p);
% Find the ID of the 3D element to which the point belongs.
boundarytoelement(i,boundaries,elements);