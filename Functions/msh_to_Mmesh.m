function [vertices, boundaries, elements, mesh] = msh_to_Mmesh(filename)
%MSH_TO_MMESH reads a .m file containing a 3D mesh.

fprintf('\nLoading mesh from file %s.m ... ',filename);

time_load = tic;
mesh_filename    =  strcat(filename,'.m');
run(mesh_filename)
vertices         =  msh.POS';
elements         =  msh.TETS';
tmp_boundaries   =  msh.TRIANGLES';

boundaries([1 2 3 12],:)   = tmp_boundaries([1 2 3 4],:);

mesh=msh;
time_load = toc(time_load);

fprintf('done in %3.2f s \n\n',time_load);

end

