function [i] = findboundary(boundaries,vertices,id,p)
%FINDBOUNDARY finds the element of the boundary with ID=id, to which the point p belongs.
for i=1:length(boundaries)
if(boundaries(12,i)==id)
i1=boundaries(1,i);
triangle(1).point.x=vertices(1,i1);
triangle(1).point.y=vertices(2,i1);

i2=boundaries(2,i);
triangle(2).point.x=vertices(1,i2);
triangle(2).point.y=vertices(2,i2);

i3=boundaries(3,i);
triangle(3).point.x=vertices(1,i3);
triangle(3).point.y=vertices(2,i3);
if(checkInside(triangle,p))
    fprintf("The point is in the boundary element: %0.f\n",i)
    return
end
end
end

