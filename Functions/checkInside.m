function [ret] = checkInside(poly,p)
%CHECKINSIDE checks if the point p is inside the triangle poly.

A=area(poly(1).point,poly(2).point,poly(3).point);

A1=area(poly(1).point,poly(2).point,p);

A2=area(poly(1).point,poly(3).point,p);

A3=area(poly(2).point,poly(3).point,p);

if(A<=A1+A2+A3 +10^-15) && (A>=A1+A2+A3 -10^-15) %% generalization of the condition A==A1+A2+A3
    ret=true;
else
    ret=false;
end
 

end

