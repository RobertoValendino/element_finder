function [A] = area(p1,p2,p3)
%AREA computes the area of the triangle given its vertices.

A=abs(p1.x*(p2.y-p3.y)+p2.x*(p3.y-p1.y)+p3.x*(p1.y-p2.y))/2.0;

end

