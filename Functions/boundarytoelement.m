function [ret] = boundarytoelement(I,boundaries,elements)
%BOUNDARYTOELEMENT returns the 3D element corresponding to the 2D boundary
%element.

for i=1:length(elements)
    count=0;
for j=1:5
    if(elements(j,i)==boundaries(1,I) || elements(j,i)==boundaries(2,I) || elements(j,i)==boundaries(3,I))
        count=count+1;
    end
if( count==3)
    fprintf("The point is in the element: %0.f\n",i)
    ret=i;
    return 
end
end
end

ret=-1;
end

