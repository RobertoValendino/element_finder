# element_finder



## Description

This repository contains some MATLAB functions that allow finding the ID of a 3D mesh element to which a point P, which lies on a surface of the domain, belongs. These functions were tested on a beam-shaped mesh. The test case implemented in the main.m code allows the user to find the ID of the mesh element to which point P=(0.5,3), which lies on surface 3, belongs.


